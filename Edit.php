<!DOCTYPE html>

<html>
<head>
  <link rel="stylesheet"  href="Style/style.css">
  <title>Edit Inventory</title>
  

</head>
<body>

<?php
  include('header.php');

?>

<div class="navbar">
  <a href="Add.php">Add</a>
  
  <div class="dropdown">
    <button class="dropbtn">View</button>
    <div class="dropdown-content">
      <a href="View_HomeFurniture.php">Home Furniture</a>
      <a href="View_Jahezpackage.php">Jahez Package</a>
      <a href="View_OfficeFurniture.php">Office Furniture</a>
      <a href="View_SchoolFurniture.php">School Furniture</a>
    </div>
  </div> 
<a href="View_Orders.php">Orders</a>
  <a href="View_Report.php">Report</a>
  <a href="View_Feedback.php">Feedback</a>
  
</div>



<div class="Sofa_back">
<section class="form">
        <form action="<?php echo"Edit.php?ID=".$_GET['ID'] ?>" method="post">
          <?php
            $sql="SELECT * FROM inventory WHERE ID =".$_GET['ID'];
            $result = $conn->query($sql);
            
            $data = $result->fetch_assoc();
 
          ?>
                <fieldset>
                <legend> Edit Inventory Form </legend>
                
                <div class="row">
                  <div class="col-20">
                     <label >Item Name : </label>
                </div>
                <div class="col-60">
                    <input type="text" name="Item_Name" value="<?php echo $data['Item_Name'] ?>">
                  </div>
                </div>
                <div class="row">
                  <div class="col-20">
                     <label >Item Price : </label>
                </div>
                <div class="col-60">
                    <input type="number" name="Item_Price" value="<?php echo $data['Item_Price'] ?>">
                  </div>
                </div>
                <div class="row">
                  <div class="col-20">
                     <label >Item Quantity : </label>
                </div>
                <div class="col-60">
                    <input type="number" name="Item_Quantity" value="<?php echo $data['Item_Quantity'] ?>">
                  </div>
                </div>
                <div class="row">
                  <div class="col-20">
                     <label >Item Detail : </label>
                </div>
                <div class="col-60">
                    <textarea id="Item_Detail" name="Item_Detail" ><?php echo $data['Item_Detail'] ?></textarea>
                  </div>
                </div>
               <div class="row">
                  <div class="col-20">
                     <label>Catogary</label>
                </div>
                <div class="col-60">
                    <select id="Item_Catogary" name="Item_Catogary">
                      <?php
                          if ($data['Item_Catogary']=='Home Furniture') 
                          {
                              echo '<option value="Home Furniture" selected>Home Furniture</option>
                                    <option value="Jahez Package">Jahez Package</option>
                                    <option value="Office Furniture">Office Furniture</option>
                                    <option value="School Furniture">School Furniture</option>';
                          }
                          elseif ($data['Item_Catogary']=='Jahez Package') 
                          {
                              echo '<option value="Home Furniture">Home Furniture</option>
                                    <option value="Jahez Package" selected>Jahez Package</option>
                                    <option value="Office Furniture">Office Furniture</option>
                                    <option value="School Furniture">School Furniture</option>';
                          }
                          elseif ($data['Item_Catogary']=='Office Furniture') 
                          {
                              echo '<option value="Home Furniture">Home Furniture</option>
                                    <option value="Jahez Package">Jahez Package</option>
                                    <option value="Office Furniture" selected>Office Furniture</option>
                                    <option value="School Furniture">School Furniture</option>';
                          }
                          else 
                          {
                              echo '<option value="Home Furniture">Home Furniture</option>
                                    <option value="Jahez Package">Jahez Package</option>
                                    <option value="Office Furniture">Office Furniture</option>
                                    <option value="School Furniture" selected>School Furniture</option>';
                          }

                      ?>


                  
                </select>
                  </div>
                </div> 
               
               
                <div class="row">
                  <div class="col-20">
                     <button class="btn1">Update</button>
                </div>
                
                
              </fieldset>
        </form>
</section>
<img src="images/2.jpeg">
 
</div>
<?php
    if(!empty($_POST['Item_Name'])
      &&!empty($_POST['Item_Price'])
      &&!empty($_POST['Item_Quantity'])
      &&!empty($_POST['Item_Detail'])
      &&!empty($_POST['Item_Catogary'])
    ){
      $Item_Name=$_POST['Item_Name'];
      $Item_Price=$_POST['Item_Price'];
      $Item_Quantity=$_POST['Item_Quantity'];
      $Item_Detail=$_POST['Item_Detail'];
      $Item_Catogary=$_POST['Item_Catogary'];

      $sql = "UPDATE inventory SET Item_Name='$Item_Name', Item_Price='$Item_Price', Item_Quantity='$Item_Quantity',Item_Detail='$Item_Detail',Item_Catogary='$Item_Catogary' WHERE ID =".$_GET['ID'];

      if ($conn->query($sql) === TRUE) {
            if($data['Item_Catogary'] =="Home Furniture")
            {
              header("Location:View_HomeFurniture.php");
            }
            elseif($data['Item_Catogary'] =="Jahez Package")
            {
              header("Location:View_Jahezpackage.php");
            }
            elseif($data['Item_Catogary'] =="Office Furniture")
            {
              header("Location:View_OfficeFurniture.php");
            }
            elseif($data['Item_Catogary'] =="School Furniture")
            {
              header("Location:View_SchoolFurniture.php");
            }
      } else {
        echo "Error: " . $sql . "<br>" . $conn->error;
      }

      $conn->close();

    }else{
      echo"Please fill the required field";
    }
?>

<footer>
  <div id="foot_copyright">
  <h5>copyright &copy; 2021</h5>
  </div>
  <div id="foot_contacts">
    <h5>
      Contact US :
    </h5>
    <p>Social Media Links</p>
  </div>
</footer>



</body>
</html>