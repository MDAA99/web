<!DOCTYPE html>

<html>
<head>
  <link rel="stylesheet"  href="Style/style.css">
  <title>Delivery Address</title>

</head>
<body>

<?php
  include('header.php');
?>

<div class="navbar">
  <a href="index.php">Home</a>
  
  <div class="dropdown">
    <button class="dropbtn">Gallary</button>
    <div class="dropdown-content">
      <a href="HomeFurniture.php">Home Furniture</a>
      <a href="Jahezpackage.php">Jahez Package</a>
      <a href="OfficeFurniture.php">Office Furniture</a>
      <a href="SchoolFurniture.php">School Furniture</a>
    </div>
  </div> 
<a href="Aboutus.php">About US</a>
  <a href="Feedback.php">Feedback</a>
  <a href="Contactus.php">Contact US</a>
  <a href="Cart.php">Cart</a>
</div>



<div class="Sofa_back">
<section class="form">
  <form action="">
    <fieldset>
    <legend> Delivery Address Details </legend>
    <div class="row">
      <div class="col-20">
        <label >First Name</label>
    </div>
    <div class="col-60">
        <input type="text"  name="firstname" placeholder="Your name..">
      </div>
    </div>
    <div class="row">
      <div class="col-20">
         <label >Last Name</label>
    </div>
    <div class="col-60">
        <input type="text" name="lastname" placeholder="Your last name..">
      </div>
    </div>
   <div class="row">
      <div class="col-20">
         <label>City</label>
    </div>
    <div class="col-60">
        <select id="city" name="city">
      <option value="Rawalpindi">Rawalpindi</option>
      <option value="Lahore">Lahore</option>
      <option value="Islamabad">Islamabad</option>
    </select>
      </div>
    </div> 
   
   <div class="row">
      <div class="col-20">
         <label >Address</label>
    </div>
    <div class="col-60">
        <textarea id="Address" name="Address" placeholder="Write something.."></textarea>
      </div>
    </div>
    <div class="row">
      <div class="col-20">
         <button class="btn1">Submit</button>
    </div>
    
    
  </fieldset>
  </form>
</section>
<img src="images/Payment.png">
 
</div>

<footer>
  <div id="foot_copyright">
  <h5>copyright &copy; 2021</h5>
  </div>
  <div id="foot_contacts">
    <h5>
      Contact US :
    </h5>
    <p>Social Media Links</p>
  </div>
</footer>



</body>
</html>