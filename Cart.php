<!DOCTYPE html>

<html>
<head>
  <link rel="stylesheet"  href="Style/style.css">
  <title>View Cart</title>

</head>
<body>

<?php
  include('header.php');
?>

<div class="navbar">
  <a href="index.php">Home</a>
  
  <div class="dropdown">
    <button class="dropbtn">Gallary</button>
    <div class="dropdown-content">
      <a href="HomeFurniture.php">Home Furniture</a>
      <a href="Jahezpackage.php">Jahez Package</a>
      <a href="OfficeFurniture.php">Office Furniture</a>
      <a href="SchoolFurniture.php">School Furniture</a>
    </div>
  </div> 
<a href="Aboutus.php">About US</a>
  <a href="Feedback.php">Feedback</a>
  <a href="Contactus.php">Contact US</a>
  <a href="Cart.php">Cart</a>
</div>



<div class="Sofa_back">
<div class="main_Body">
  <section class="form">
  <table>
          <thead>
            <tr>
              <th>Item ID</th>
              <th>Product Name</th>
              <th>Item Detail</th>
              <th>Item Price</th>
              <th>Item catogary</th>
              <th>Order status</th>
              <th colspan="2">Action</th>
            </tr>
          </thead>
  <?php
  $page="Cart.php";
            $sql="SELECT * FROM cart";

            $result =$conn->query($sql);

            if($result->num_rows > 0)
            {
              while($row = $result->fetch_assoc()){

                echo" <tr>
                        <td>".$row['Item_Id']."</td>
                        <td>".$row['Order_name']."</td>
                        <td>".$row['Order_detail']."</td>
                        <td>".$row['Order_price']."</td>
                        <td>".$row['Order_catogary']."</td>
                        <td>".$row['Order_status']."</td>
                        <td>
                         <a href='Cart_order.php?Order_Id=".$row['Order_Id']."' class='edit_btn' >Order</a>
                        </td>
                        <td>
                          <a href='Cart_delete.php?Page=".$page."&&Order_Id=".$row['Order_Id']."' class='del_btn'>Remove</a>
                        </td>
                      </tr>";
              }
            }
            else
            {
              echo"<tr><td>Inventory is Empty</td></tr>";
            }
          ?>
        </table>
      </section>
</div>
<img src="images/cart.jpg">
 
</div>

<footer>
  <div id="foot_copyright">
  <h5>copyright &copy; 2021</h5>
  </div>
  <div id="foot_contacts">
    <h5>
      Contact US :
    </h5>
    <p>Social Media Links</p>
  </div>
</footer>



</body>
</html>