-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 17, 2021 at 06:55 PM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 8.0.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `furnituregallary`
--

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `Order_Id` int(11) NOT NULL,
  `Item_Id` int(11) NOT NULL,
  `Order_name` varchar(100) NOT NULL,
  `Order_detail` longtext NOT NULL,
  `Order_price` int(11) NOT NULL,
  `Order_catogary` varchar(100) NOT NULL,
  `Order_status` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`Order_Id`, `Item_Id`, `Order_name`, `Order_detail`, `Order_price`, `Order_catogary`, `Order_status`) VALUES
(41, 29, 'Dining table', 'Round wooden table with 6 seats polished wood', 25000, 'Home Furniture', 'Order confirmed You will recive in a Week'),
(42, 25, 'Bed', 'Single bed wooden made durable with spring foam\r\nand 2 bed sheet', 20000, 'Home Furniture', 'Check');

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE `feedback` (
  `ID` int(11) NOT NULL,
  `firstname` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `city` varchar(100) NOT NULL,
  `subject` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `inventory`
--

CREATE TABLE `inventory` (
  `ID` int(11) NOT NULL,
  `Item_Name` varchar(100) NOT NULL,
  `Item_Price` int(11) NOT NULL,
  `Item_Quantity` int(11) NOT NULL,
  `Item_Detail` longtext NOT NULL,
  `Item_Catogary` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `inventory`
--

INSERT INTO `inventory` (`ID`, `Item_Name`, `Item_Price`, `Item_Quantity`, `Item_Detail`, `Item_Catogary`) VALUES
(23, 'Sofa', 10000, 15, '2 seat sofa blue colour\r\nwith 2 blanket', 'Jahez Package'),
(26, 'Almira', 30000, 23, 'Metal Almira 3 door with safety lock system and safe', 'School Furniture'),
(29, 'Dining table', 25000, 40, 'Round wooden table with 6 seats polished wood', 'Home Furniture'),
(30, 'PC Table', 15000, 85, 'Wood top and metal legs\r\neasy to assemble best for gaming PC  ', 'Office Furniture'),
(31, 'zsdjh', 345, 4, 'aszfv', 'Jahez Package');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `pass` varchar(50) NOT NULL,
  `cp` varchar(50) NOT NULL,
  `url` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `email`, `pass`, `cp`, `url`) VALUES
(1, 'usama', 'usama@gmail.com', 'f970e2767d0cfe75876ea857f92e319b', 'f970e2767d0cfe75876ea857f92e319b', ''),
(2, 'usama', 'usama@gmail.com', 'f970e2767d0cfe75876ea857f92e319b', 'f970e2767d0cfe75876ea857f92e319b', 'uploads/'),
(3, 'usama', 'usama@gmail.com', 'f970e2767d0cfe75876ea857f92e319b', 'f970e2767d0cfe75876ea857f92e319b', 'uploads/1432254895711.jpg'),
(4, 'mdaa', 'mdaa@gmail.com', 'f970e2767d0cfe75876ea857f92e319b', 'f970e2767d0cfe75876ea857f92e319b', 'uploads/Snapshot_20200301205931.png');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`Order_Id`);

--
-- Indexes for table `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `inventory`
--
ALTER TABLE `inventory`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `Order_Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `feedback`
--
ALTER TABLE `feedback`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `inventory`
--
ALTER TABLE `inventory`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
